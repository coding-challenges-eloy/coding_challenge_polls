import React from 'react'
import renderer from 'react-test-renderer'
import { QuestionsContainer } from './Questions'

it('QuestionsContainer renders correctly', () => {
  const tree = renderer
    .create(<QuestionsContainer fetchQuestions={() => {}} />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})
