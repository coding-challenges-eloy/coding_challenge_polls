import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchQuestions } from '../ducks/questions'
import { Questions } from '../components'
import { Container, Header } from 'semantic-ui-react'

export class QuestionsContainer extends Component {
  // Lifecycle methods

  componentDidMount () {
    this.props.fetchQuestions()
  }

  // render

  render () {
    const { questions } = this.props
    return (
      <Container>
        <Header as='h1'>Questions</Header>
        <Questions questions={questions || []} />
      </Container>
    )
  }
}

const mapStateToProps = state => state

const mapDispatchToProps = dispatch => ({
  fetchQuestions: () => dispatch(fetchQuestions())
})

export default connect(mapStateToProps, mapDispatchToProps)(QuestionsContainer)
