import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import QuestionsContainer from './containers/Questions'
import store from './store'
import registerServiceWorker from './registerServiceWorker'
import 'semantic-ui-css/semantic.min.css'

ReactDOM.render(
  <Provider store={store}>
    <QuestionsContainer />
  </Provider>,
  document.getElementById('root')
)

registerServiceWorker()
