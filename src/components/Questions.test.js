import React from 'react'
import renderer from 'react-test-renderer'
import Questions from './Questions'

it('Questions renders correctly', () => {
  const tree = renderer
    .create(<Questions questions={[]} />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})
