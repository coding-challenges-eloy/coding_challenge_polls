import React from 'react'
import renderer from 'react-test-renderer'
import Question from './Question'

it('Question renders correctly', () => {
  const tree = renderer
    .create(<Question choices={[]} />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})
