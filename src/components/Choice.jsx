import React from 'react'

const Choice = ({choice, votes}) => (
  <li><b>{choice}</b> has {votes} votes</li>
)

export default Choice
