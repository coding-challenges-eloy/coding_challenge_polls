import React from 'react'
import renderer from 'react-test-renderer'
import Choice from './Choice'

it('Choice renders correctly', () => {
  const tree = renderer
    .create(<Choice />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})
