import React from 'react'
import { Card } from 'semantic-ui-react'
import { Question } from '.'

const Questions = ({questions}) => (
  <Card.Group>
    {questions.map(
      (question, i) => <Question key={i} {...question} />
    )}
  </Card.Group>
)

export default Questions
