import React from 'react'
import { Card } from 'semantic-ui-react'
import { Choice } from '.'
import { formatDate } from '../util'

const Question = ({choices, question, published_at}) => (
  <Card>
    <Card.Content>
      <Card.Header>{question}</Card.Header>
      <Card.Meta>{formatDate(published_at)}</Card.Meta>
      <Card.Description>
        {choices.map(
          (choice, i) => <Choice key={i} {...choice} />
        )}
      </Card.Description>
    </Card.Content>
  </Card>
)

export default Question
