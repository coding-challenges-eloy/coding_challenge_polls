import reducer from '.'
import { RECEIVE_QUESTIONS } from './questions'

describe('breeds reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      'questions': []
    })
  })

  it('should handle RECEIVE_QUESTIONS', () => {
    const FIXTURE = [
      {
        'choices': [
          {
            'choice': 'Cyan',
            'url': '/questions/6/choices/50',
            'votes': 0
          }, {
            'choice': 'Green',
            'url': '/questions/6/choices/49',
            'votes': 0
          }
        ],
        'published_at': '2015-05-27T21:22:26.576000+00:00',
        'question': 'Favourite colour?',
        'url': '/questions/6'
      }, {
        'choices': [
          {
            'choice': '🇨🇦',
            'url': '/questions/7/choices/55',
            'votes': 0
          }, {
            'choice': '🇬🇧',
            'url': '/questions/7/choices/53',
            'votes': 0
          }, {
            'choice': '🇺🇸',
            'url': '/questions/7/choices/54',
            'votes': 0
          }
        ],
        'published_at': '2015-05-27T21:22:26.601000+00:00',
        'question': 'Bacon?',
        'url': '/questions/7'
      }
    ]

    expect(
      reducer(undefined, {
        type: RECEIVE_QUESTIONS,
        payload: FIXTURE
      })
    ).toEqual({
      'questions': FIXTURE
    })
  })

  // TODO: write tests for the rest of reducers
})
