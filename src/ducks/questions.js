/**
 * @module ducks/questions
 *
 * This module follows
 * {@link https://github.com/erikras/ducks-modular-redux|ducks propsal}
 * to orginise actions, reducers and epics.
 */
import { handleError } from '../util'
const fetch = window.fetch

// ACTIONS
export const REQUEST_QUESTIONS = 'polls/questions/REQUEST_QUESTIONS'
export const RECEIVE_QUESTIONS = 'polls/questions/RECEIVE_QUESTIONS'
export const FAILURE_QUESTIONS = 'polls/questions/FAILURE_QUESTIONS'

// REDUCER

/**
 * Object describing the initial state
 * @type {Object}
 */
const initialState = {
  questions: []
}

/**
 * @function reducer
 * @param {Object} [state=initialState]
 * @param {FSA} action @see {@link https://github.com/acdlite/flux-standard-action|FSA}
 * @return {Object}
 */
export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case RECEIVE_QUESTIONS:
      return Object.assign({}, state, {
        questions: action.payload
      })
    case FAILURE_QUESTIONS:
      return Object.assign({}, state, {
        error: action.error && action.payload
      })
    default:
      return state
  }
}

// ACTION CREATORS

export const receiveQuestions = (payload) => ({
  type: RECEIVE_QUESTIONS,
  payload
})

export const failureQuestions = (error) => ({
  type: FAILURE_QUESTIONS,
  payload: error,
  error: true
})

// SIDE-EFFECTS

// side-effect: HTTP request
export const fetchQuestions = () => {
  return (dispatch) => {
    return fetch('https://polls.apiblueprint.org/questions')
      .then(handleError)
      .then(payload => dispatch(receiveQuestions(payload)))
      .catch(error => dispatch(failureQuestions(error)))
  }
}

// helpers
