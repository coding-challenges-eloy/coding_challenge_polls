import { default as questionsReducer } from './questions'

// normally we'll use `combineReducers` here
// as far as we only have one reducer
// and to keep example simple
// we make questionsReducers as rootReducer
export default questionsReducer
